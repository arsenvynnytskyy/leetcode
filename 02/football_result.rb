def football_result(predicted_score, real_score)
  predicted_home_goals, predicted_away_goals = predicted_score.split(':').map(&:to_i)
  real_home_goals, real_away_goals = real_score.split(':').map(&:to_i)

  if predicted_home_goals == real_home_goals && predicted_away_goals == real_away_goals
    return 2  # Big prize for an exact match
  elsif (predicted_home_goals > predicted_away_goals && real_home_goals > real_away_goals) ||
        (predicted_home_goals < predicted_away_goals && real_home_goals < real_away_goals) ||
        (predicted_home_goals == predicted_away_goals && real_home_goals == real_away_goals)
    return 1  # Small prize for predicting the winner or a draw
  else
    return 0  # Zero prize for incorrect prediction
  end
end