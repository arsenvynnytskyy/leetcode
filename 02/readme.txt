The task is about football.
Two teams playing soccer. 
Someone bets on the outcome of the match, for example, 1:2.
At the end of the match, the score of the match becomes clear and we need to give out a prize. 
If someone guesses the exact score, they get a big prize. 
If someone guesses the winner (a team wins or draws), they get a small prize. 
If they don't, they get a zero prize.
You need to write a function in your favorite programming language that takes as arguments the predicted score 
and the real score and returns an integer 0, 1, or 2 (zero, small or large prize).