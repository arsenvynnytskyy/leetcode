# @param {Integer} x
# @return {Boolean}
def is_palindrome(x)
  return false if x < 0 || (x % 10 == 0 && x != 0)

  reversed = 0
  original = x

  while x > 0
    digit = x % 10
    x /= 10
    reversed = reversed * 10 + digit
  end

  original == reversed
end
