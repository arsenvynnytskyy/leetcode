# @param {Integer} n
# @return {String[]}
def fizz_buzz(n)
  answer = (1..n).map do |i|
    case
    when i % 3 == 0 && i % 5 == 0 then 'FizzBuzz'
    when i % 3 == 0 then 'Fizz'
    when i % 5 == 0 then 'Buzz'
    else i.to_s
    end
  end

  answer
end
